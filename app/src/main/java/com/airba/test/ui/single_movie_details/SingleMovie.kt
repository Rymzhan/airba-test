package com.airba.test.ui.single_movie_details

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.airba.test.R
import com.airba.test.data.api.POSTER_URL
import com.airba.test.data.api.TheMovieDBClient
import com.airba.test.data.api.TheMovieDBInterface
import com.airba.test.data.repository.NetworkState
import com.airba.test.data.vo.MovieDetails
import kotlinx.android.synthetic.main.activity_single_movie.*
import java.text.NumberFormat
import java.util.*

class SingleMovie : AppCompatActivity() {

    private lateinit var viewModel: SingleMovieViewModel
    private lateinit var movieRepository: MovieDetailsRepository


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_single_movie)

        initSupportActionBar()

        val movieId: Int = intent.getIntExtra("id", 1)

        val apiService: TheMovieDBInterface = TheMovieDBClient.getClient()
        movieRepository = MovieDetailsRepository(apiService)

        viewModel = getViewModel(movieId)

        viewModel.movieDetails.observe(this, Observer {
            bindUI(it)
        })

        viewModel.networkState.observe(this, Observer {
            progress_bar.visibility = if (it == NetworkState.LOADING) View.VISIBLE else View.GONE
            txt_error.visibility = if (it == NetworkState.ERROR) View.VISIBLE else View.GONE

            if (it == NetworkState.ERROR)
                supportActionBar?.show()
        })

    }

    private fun initSupportActionBar() {
        supportActionBar?.hide()
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    fun bindUI(movieData: MovieDetails) {
        movie_title.text = movieData.title
        movie_tagline.text = movieData.tagline
        movie_release_date.text = movieData.releaseDate
        movie_rating.text = movieData.rating.toString()
        movie_runtime.text = movieData.runtime.toString() + " minutes"
        movie_overview.text = movieData.overview

        val formatCurrency = NumberFormat.getCurrencyInstance(Locale.US)
        movie_budget.text = formatCurrency.format(movieData.budget)
        movie_revenue.text = formatCurrency.format(movieData.revenue)

        val moviePosterURL = POSTER_URL + movieData.posterPath
        Glide.with(this)
            .load(moviePosterURL)
            .into(iv_movie_poster)


        supportActionBar?.setTitle(movieData.title)
        supportActionBar?.show()
    }


    private fun getViewModel(movieId: Int): SingleMovieViewModel {
        return ViewModelProviders.of(this, object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                @Suppress("UNCHECKED_CAST")
                return SingleMovieViewModel(movieRepository, movieId) as T
            }
        })[SingleMovieViewModel::class.java]
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

}
